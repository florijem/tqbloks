tqbloks
=======

Se prononce `toublox` en français ou `tooblox` en anglais.

Il s'agit d'une bibliothèque de fonctions en PHP pour programmer de  
façon fonctionnelle.

Usage
-----

La version minimale `7.0.0` de PHP est requise.

Inclure `tqbloks` dans un source PHP avec l'une des possibilités  
suivantes :

```php
include '/chemin/tqbloks.php';
include_once '/chemin/tqbloks.php';
require '/chemin/tqbloks.php';
require_once '/chemin/tqbloks.php';
```

Créer une instance de `tqbloks` :

```php
$t = new Tqbloks();
```

Documentation
-------------

- ternary :

    ```php
    // $t->ternary( boolean [ , mixed = true [ , mixed = false ]] ) -> mixed
    $t->ternary( $expression );
    $t->ternary( $expression , $value_if_true );
    $t->ternary( $expression , $value_if_true , $value_if_false );
    ```

- is_yep :

    ```php
    // $t->is_yep ( boolean ) -> boolean
    $t->is_yep ( $expression );
    ```

- is_nop :

    ```php
    // $t->is_nop ( boolean ) -> boolean
    $t->is_nop ( $expression );
    ```

- exists :

    ```php
    // $t->exists ( mixed ) -> boolean
    $t->exists ( $variable );
    ```

- is_empty :

    ```php
    // $t->is_empty ( mixed ) -> boolean
    $t->is_empty ( $variable );
    ```

- is_full :

    ```php
    // $t->is_full ( mixed ) -> boolean
    $t->is_full ( $variable );
    ```

- build_func :

    ```php
    // $t->build_func ( function , array ) -> function
    $t->build_func ( $function , $args_array );
    ```

- do_if_yep :

    ```php
    // $t->do_if_yep ( boolean , function [ , ...array ] ) -> mixed || false
    $t->do_if_yep ( $expression , $function );
    $t->do_if_yep ( $expression , $function , ...$args_array );
    ```

- do_if_nop :

    ```php
    // $t->do_if_nop ( boolean , function [ , ...array ] ) -> mixed || false
    $t->do_if_nop ( $expression , $function );
    $t->do_if_nop ( $expression , $function , ...$args_array );
    ```

- exists_key :

    ```php
    // $t->exists_key ( array , string ) -> boolean
    $t->exists_key ( $array , $key );
    ```

- add_val :

    ```php
    // $t->add_val ( array , mixed [ , string = null ] ) -> array
    $t->add_val ( $array , $value );
    $t->add_val ( $array , $value , $key );
    ```

- get_val :

    ```php
    // $t->get_val ( array , string ) -> mixed || false
    $t->get_val ( $array  , $key );
    ```

- exist_keys :

    ```php
    // $t->exist_keys ( array , array ) -> array [ boolean ]
    $t->exist_keys ( $array , $keys );
    ```

- are_equal :

    ```php
    // $t->are_equal ( array [ , mixed = null ] ) -> boolean
    $t->are_equal ( $array );
    $t->are_equal ( $array , $search_value );
    ```

- get_var :

    ```php
    // $t->get_var ( string [ , string = null ] ) -> mixed
    $t->get_var ( $name );
    $t->get_var ( $name , $path );
    ```

- is_ text :

    ```php
    // $t->is_text ( mixed ) -> boolean
    $t->is_text ( $text );
    ```

- delete_space :

    ```php
    // $t->delete_space ( string ) -> string
    $t->delete_space ( $text );
    ```

- lenght :

    ```php
    // $t->lenght ( string ) -> integer
    $t->lenght ( $text );
    ```

- upperize :

    ```php
    // $t->upperize ( string ) -> string
    $t->upperize ( $text );
    ```

- lowerize :

    ```php
    // $t->lowerize ( string ) -> string
    $t->lowerize ( $text );
    ```

- capitalize :

    ```php
    // $t->capitalize ( string ) -> string
    $t->capitalize ( $text );
    ```

- wrap_text :

    ```php
    // $t->wrap_text ( string [ , integer = 70 [ , string = "\r\n" ]] )
    // -> string
    $t->wrap_text ( $text );
    $t->wrap_text ( $text , $limit );
    $t->wrap_text ( $text , $limit , $cut );
    ```

- trigger_exception :

    ```php
    // $t->trigger_exception ( string , string [ , integer = 0 ] )
    // -> throw exception
    $t->trigger_exception ( $name , $message );
    $t->trigger_exception ( $name , $message , $code );
    ```

- email :

    ```php
    // $t->email
    //   ( string
    //   , string
    //   , string
    //   , string
    //   , string
    //   [ , string = null ]
    //   )
    // -> boolean
    $t->email ( $to , $subject , $message , $from_name , $from );
    $t->email ( $to , $subject , $message , $from_name , $from , $reply_to );
    ```

- always :

    ```php
    // $t->always ( mixed ) -> mixed
    $t->always ( $variable );
    ```

- webify :

    ```php
    // $t->webify ( string ) -> string
    $t->webify ( $text );
    ```

- webify_html

    ```php
    // $t->webify_html ( string ) -> string
    $t->webify_html ( $text );
    ```

- paragraph :

    ```php
    // $t->paragraph ( array , string [ , string = '' [ , string = '' ]] )
    // -> string
    $t->paragraph ( $array , $separator );
    $t->paragraph ( $array , $separator , $prefix );
    $t->paragraph ( $array , $separator , $prefix , $suffix );
    ```

- lotto :

    ```php
    // $t->lotto ( integer [ , integer = 0 [ , integer = 99 ]] )
    // -> array [ integer ]
    $t->lotto ( $number_number );
    $t->lotto ( $number_number , $minimum );
    $t->lotto ( $number_number , $minimum , $maximum );
    ```

- captcha :

    ```php
    // $t->captcha ( [ string = null ] )
    // -> array
    //      [ 'chosen' => integer
    //      , 'column' => integer
    //      , 'row'    => integer
    //      , 'matrix' => string
    //      ]
    $t->captcha ();
    $t->captcha ( $title );
    ```

Licence
-------

Copyright ou © ou Copr. Florijem, <florijem@ideovif.net>, 16-08-2018. 

Cette bibliothèque est un ensemble de fonctions en PHP pour programmer  
de façon fonctionnelle.

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et  
respectant les principes de diffusion des logiciels libres. Vous pouvez  
utiliser, modifier et/ou redistribuer ce programme sous les conditions  
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA  
sur le site <http://www.cecill.info>.

En contrepartie de l'accessibilité au code source et des droits de copie,  
de modification et de redistribution accordés par cette licence, il n'est  
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,  
seule une responsabilité restreinte pèse sur l'auteur du programme,  le  
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques  
associés au chargement,  à l'utilisation,  à la modification et/ou au  
développement et à la reproduction du logiciel par l'utilisateur étant  
donné sa spécificité de logiciel libre, qui peut le rendre complexe à  
manipuler et qui le réserve donc à des développeurs et des professionnels  
avertis possédant  des  connaissances  informatiques approfondies.  Les  
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du  
logiciel à leurs besoins dans des conditions permettant d'assurer la  
sécurité de leurs systèmes et ou de leurs données et, plus généralement,  
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez  
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les  
termes.
