tqbloks
=======

Is pronounced `toublox` in french or `tooblox` in english.

This is a library of functions in PHP to program in functional way.

Usage
-----

The minimal PHP version `7.0.0` is required.

Include `tqbloks` in a PHP source with one of the following  
possibilities :

```php
include '/path/tqbloks.php';
include_once '/path/tqbloks.php';
require '/path/tqbloks.php';
require_once '/path/tqbloks.php';
```

Create a new instance of `tqbloks` :

```php
$t = new Tqbloks();
```

Documentation
-------------

- ternary :

    ```php
    // $t->ternary( boolean [ , mixed = true [ , mixed = false ]] ) -> mixed
    $t->ternary( $expression );
    $t->ternary( $expression , $value_if_true );
    $t->ternary( $expression , $value_if_true , $value_if_false );
    ```

- is_yep :

    ```php
    // $t->is_yep ( boolean ) -> boolean
    $t->is_yep ( $expression );
    ```

- is_nop :

    ```php
    // $t->is_nop ( boolean ) -> boolean
    $t->is_nop ( $expression );
    ```

- exists :

    ```php
    // $t->exists ( mixed ) -> boolean
    $t->exists ( $variable );
    ```

- is_empty :

    ```php
    // $t->is_empty ( mixed ) -> boolean
    $t->is_empty ( $variable );
    ```

- is_full :

    ```php
    // $t->is_full ( mixed ) -> boolean
    $t->is_full ( $variable );
    ```

- build_func :

    ```php
    // $t->build_func ( function , array ) -> function
    $t->build_func ( $function , $args_array );
    ```

- do_if_yep :

    ```php
    // $t->do_if_yep ( boolean , function [ , ...array ] ) -> mixed || false
    $t->do_if_yep ( $expression , $function );
    $t->do_if_yep ( $expression , $function , ...$args_array );
    ```

- do_if_nop :

    ```php
    // $t->do_if_nop ( boolean , function [ , ...array ] ) -> mixed || false
    $t->do_if_nop ( $expression , $function );
    $t->do_if_nop ( $expression , $function , ...$args_array );
    ```

- exists_key :

    ```php
    // $t->exists_key ( array , string ) -> boolean
    $t->exists_key ( $array , $key );
    ```

- add_val :

    ```php
    // $t->add_val ( array , mixed [ , string = null ] ) -> array
    $t->add_val ( $array , $value );
    $t->add_val ( $array , $value , $key );
    ```

- get_val :

    ```php
    // $t->get_val ( array , string ) -> mixed || false
    $t->get_val ( $array  , $key );
    ```

- exist_keys :

    ```php
    // $t->exist_keys ( array , array ) -> array [ boolean ]
    $t->exist_keys ( $array , $keys );
    ```

- are_equal :

    ```php
    // $t->are_equal ( array [ , mixed = null ] ) -> boolean
    $t->are_equal ( $array );
    $t->are_equal ( $array , $search_value );
    ```

- get_var :

    ```php
    // $t->get_var ( string [ , string = null ] ) -> mixed
    $t->get_var ( $name );
    $t->get_var ( $name , $path );
    ```

- is_ text :

    ```php
    // $t->is_text ( mixed ) -> boolean
    $t->is_text ( $text );
    ```

- delete_space :

    ```php
    // $t->delete_space ( string ) -> string
    $t->delete_space ( $text );
    ```

- lenght :

    ```php
    // $t->lenght ( string ) -> integer
    $t->lenght ( $text );
    ```

- upperize :

    ```php
    // $t->upperize ( string ) -> string
    $t->upperize ( $text );
    ```

- lowerize :

    ```php
    // $t->lowerize ( string ) -> string
    $t->lowerize ( $text );
    ```

- capitalize :

    ```php
    // $t->capitalize ( string ) -> string
    $t->capitalize ( $text );
    ```

- wrap_text :

    ```php
    // $t->wrap_text ( string [ , integer = 70 [ , string = "\r\n" ]] )
    // -> string
    $t->wrap_text ( $text );
    $t->wrap_text ( $text , $limit );
    $t->wrap_text ( $text , $limit , $cut );
    ```

- trigger_exception :

    ```php
    // $t->trigger_exception ( string , string [ , integer = 0 ] )
    // -> throw exception
    $t->trigger_exception ( $name , $message );
    $t->trigger_exception ( $name , $message , $code );
    ```

- email :

    ```php
    // $t->email
    //   ( string
    //   , string
    //   , string
    //   , string
    //   , string
    //   [ , string = null ]
    //   )
    // -> boolean
    $t->email ( $to , $subject , $message , $from_name , $from );
    $t->email ( $to , $subject , $message , $from_name , $from , $reply_to );
    ```

- always :

    ```php
    // $t->always ( mixed ) -> mixed
    $t->always ( $variable );
    ```

- webify :

    ```php
    // $t->webify ( string ) -> string
    $t->webify ( $text );
    ```

- webify_html

    ```php
    // $t->webify_html ( string ) -> string
    $t->webify_html ( $text );
    ```

- paragraph :

    ```php
    // $t->paragraph ( array , string [ , string = '' [ , string = '' ]] )
    // -> string
    $t->paragraph ( $array , $separator );
    $t->paragraph ( $array , $separator , $prefix );
    $t->paragraph ( $array , $separator , $prefix , $suffix );
    ```

- lotto :

    ```php
    // $t->lotto ( integer [ , integer = 0 [ , integer = 99 ]] )
    // -> array [ integer ]
    $t->lotto ( $number_number );
    $t->lotto ( $number_number , $minimum );
    $t->lotto ( $number_number , $minimum , $maximum );
    ```

- captcha :

    ```php
    // $t->captcha ( [ string = null ] )
    // -> array
    //      [ 'chosen' => integer
    //      , 'column' => integer
    //      , 'row'    => integer
    //      , 'matrix' => string
    //      ]
    $t->captcha ();
    $t->captcha ( $title );
    ```

License
-------

Copyright or © or Copr. Florijem, <florijem@ideovif.net>, 16-08-2018. 

This library is a set of functions in PHP to program in functional way.

This software is governed by the CeCILL-B license under French law and  
abiding by the rules of distribution of free software.  You can  use,  
modify and/ or redistribute the software under the terms of the CeCILL-B  
license as circulated by CEA, CNRS and INRIA at the following URL  
<http://www.cecill.info>.

As a counterpart to the access to the source code and  rights to copy,  
modify and redistribute granted by the license, users are provided only  
with a limited warranty  and the software's author,  the holder of the  
economic rights,  and the successive licensors  have only  limited  
liability.

In this respect, the user's attention is drawn to the risks associated  
with loading,  using,  modifying and/or developing or reproducing the  
software by the user in light of its specific status of free software,  
that may mean  that it is complicated to manipulate,  and  that  also  
therefore means  that it is reserved for developers  and  experienced  
professionals having in-depth computer knowledge. Users are therefore  
encouraged to load and test the software's suitability as regards their  
requirements in conditions enabling the security of their systems and/or  
data to be ensured and,  more generally, to use and operate it in the  
same conditions as regards security.

The fact that you are presently reading this means that you have had  
knowledge of the CeCILL-B license and that you accept its terms.
