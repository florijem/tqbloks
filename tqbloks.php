<?php
// tqbloks
// Copyright or © or Copr. Florijem, florijem@ideovif.net, 16-08-2018. 
// This library is a set of functions in PHP to program in functional way.
// This software is governed by the CeCILL-B license under French law and  
// abiding by the rules of distribution of free software.  You can  use,  
// modify and/ or redistribute the software under the terms of the CeCILL-B  
// license as circulated by CEA, CNRS and INRIA at the following URL  
// http://www.cecill.info.
// As a counterpart to the access to the source code and  rights to copy,  
// modify and redistribute granted by the license, users are provided only  
// with a limited warranty  and the software's author,  the holder of the  
// economic rights,  and the successive licensors  have only  limited  
// liability.
// In this respect, the user's attention is drawn to the risks associated  
// with loading,  using,  modifying and/or developing or reproducing the  
// software by the user in light of its specific status of free software,  
// that may mean  that it is complicated to manipulate,  and  that  also  
// therefore means  that it is reserved for developers  and  experienced  
// professionals having in-depth computer knowledge. Users are therefore  
// encouraged to load and test the software's suitability as regards their  
// requirements in conditions enabling the security of their systems and/or  
// data to be ensured and,  more generally, to use and operate it in the  
// same conditions as regards security.
// The fact that you are presently reading this means that you have had  
// knowledge of the CeCILL-B license and that you accept its terms.
  
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
mb_http_input('UTF-8');
mb_language('uni');
mb_regex_encoding('UTF-8');
ob_start('mb_output_handler');

class Tqbloks
{ function ternary 
    ( $expression, $value_if_true = true, $value_if_false = false )
  { $value_if_true_is_function =
      is_callable($value_if_true);
    $value_if_false_is_function =
      is_callable($value_if_false);
    return
      ( $expression
        ? ( $value_if_true_is_function
            ? $value_if_true()
            : $value_if_true
          )
        : ( $value_if_false_is_function
            ? $value_if_false()
            : $value_if_false
          )
      );
  }
  function is_yep ( $expression )
  { return ( is_bool($expression)  && ( $expression === true ) );
  }
  function is_nop ( $expression ) 
  { return ( is_bool($expression) && ( $expression === false ) );
  }
  function exists ( $variable )
  { return isset($variable);
  }
  function is_empty ( $variable )
  { return $this->ternary($this->exists($variable), empty($variable), true);
  }
  function is_full ( $variable )
  { return $this->is_nop($this->is_empty($variable));
  }
  function build_func ( $function, $args_array )
  { return
      ( function () use ( $function, $args_array )
        { return call_user_func_array($function, $args_array);
        }
      );
  }
  function do_if_yep ( $expression, $function, ...$args_array )
  { $func_builded =
      $this->build_func($function, $args_array);
    return $this->ternary($this->is_yep($expression), $func_builded);
  }
  function do_if_nop ( $expression, $function, ...$args_array )
  { $func_builded =
      $this->build_func($function, $args_array);
    return $this->ternary($this->is_nop($expression), $func_builded);
  }
  function exists_key ( $array, $key )
  { return array_key_exists($key, $array);
  }
  function add_val ( $array, $value, $key = null )
  { if ( $this->exists($key) )
    { $array[$key] =
        $value;
    } else
    { $array[] =
        $value;
    }

    return $array;
  }
  function get_val ( $array, $key )
  { if ( $this->exists_key($array, $key) )
    { return $array[$key];
    } else
    { return false;
    }
  }
  function exist_keys ($array, $keys )
  { $exist_keys_recursive = 
      function ( $array, $keys, $array_output )
      use ( &$exist_keys_recursive )
      { $count_output =
          count($array_output);

        if ( count($keys) === $count_output )
        { return $array_output;
        } else
        { $array_output[$keys[$count_output]] =
            $this->exists_key($array, $keys[$count_output]);
          return
            $exist_keys_recursive($array, $keys, $array_output);
        }
      };

    return $exist_keys_recursive($array, $keys, array());
  }
  function are_equal ( $array, $search_value = null )
  { $array_no_double =
      array_unique($array);
    return
      $this->ternary
        ( $this->exists($search_value)
        , (  ( count($array_no_double) === 1 )
          && ( $search_value === array_values($array_no_double)[0] )
          )
        , ( count($array_no_double) === 1 )
        );
  }
  function get_var ( $name, $path = null )
  { if ( $this->is_full($path) && file_exists($path) )
    { include "$path";
    }

    return ${$name};
  }
  function is_text ( $text )
  { return is_string($text);
  }
  function delete_space ( $text )
  { return $this->ternary
      ( $this->is_text($text)
      , mb_ereg_replace("\s{1,}"," ", trim($text))
      );
  }
  function lenght ( $text )
  { return $this->ternary($this->is_text($text), mb_strlen($text));
  }
  function upperize ( $text )
  { return $this->ternary($this->is_text($text), mb_strtoupper($text));
  }
  function lowerize ( $text )
  { return $this->ternary( $this->is_text($text), mb_strtolower($text));
  }
  function capitalize ( $text )
  { return
      $this->ternary
        ( ( $this->is_nop($this->is_empty($text)) && $this->is_text($text) )
        , ( $this->upperize(mb_substr($text, 0, 1))
          . mb_substr($text, 1, $this->lenght($text))
          )
        );
  }
  function wrap_text ( $text, $limit = 70, $cut = "\r\n" )
  { $text_no_space =
      $delete_space($text);
    $real_limit =
      ( $limit - $this->lenght($cut) );

    if ( $this->is_empty($text_no_space) )
    { return $text_no_space;
    } elseif ( $this->lenght($text_no_space) <= $limit )
    { return $text_no_space;
    } else
    { $current_line =
        '';
      $line_array =
        [];
      $word_array =
        explode(' ', $text_no_space);
      $lenght_word_array =
        count($word_array);

      foreach ( $word_array as $key => $word )
      { $lenght_current_line =
          $this->lenght($current_line);
        $lenght_word =
          $this->lenght($word);
        $is_last_word =
          ( ( $key + 1 ) === $lenght_word_array );
        $current_limit =
          $this->ternary($is_last_word, $limit, $real_limit);

        if ( $this->is_empty($current_line) )
        { if ( ( $lenght_word >= $current_limit ) || $is_last_word )
          { $line_array[] =
              $word;
          } else
          { $current_line =
              $word;
          }
        } elseif ( ( $lenght_current_line + 1 + $lenght_word ) > $current_limit )
        { $line_array[] =
            $current_line;

          if ( $is_last_word )
          { $line_array[] =
              $word;
          } else
          { $current_line =
              $word;
          }
        } else
        { if ( $is_last_word )
          { $line_array[] =
              ( $current_line . ' ' . $word );
          } else
          { $current_line =
              ( $current_line . ' ' . $word );
          }
        }
      }
    }

    return implode($cut, $line_array);
  }
  function trigger_exception ( $name, $message, $code = 0 )
  { throw new $name($message, $code);
  }
  function email
    ( $to
    , $subject
    , $message
    , $from_name
    , $from
    , $reply_to = null
    )
  { $replyto =
      $this->ternary($this->exists($reply_to), $reply_to, $from);
    $subject_utf8 =
      "=?utf-8?b?" . base64_encode($subject) . "?=";
    $from_name_utf8 =
      "=?utf-8?b?" . base64_encode($from_name) . "?=";
    $headers =
      "MIME-Version: 1.0\r\n"
      . "From: $from_name_utf8  <$from>\r\n"
      . "Content-Type: text/plain;charset=utf-8\r\n"
      . "Reply-To: $replyto\r\n"
      . "X-Mailer: PHP/" . phpversion();
    return mail($to, $subject_utf8, $message, $headers);
  }
  function always ( $variable )
  { return $variable;
  }
  function webify ( $text )
  { $text_webified =
      htmlentities
        ( trim(strip_tags(stripslashes($text)))
        , ENT_QUOTES | ENT_HTML401
        , 'UTF-8'
        );
    return $text_webified;
  }
  function webify_html ( $text )
  { $text_webified =
      strip_tags
        ( htmlentities
            ( trim(stripslashes($text))
            , ENT_QUOTES | ENT_HTML401
            , "UTF-8"
            )
        );
    return $text_webified;
  }
  function paragraph ( $array, $separator, $prefix = '', $suffix = '' )
  { return
      ( $prefix . implode($separator, $array) . $suffix );
  }
  function lotto ( $number_number, $minimum = 0, $maximum = 99 )
  { $array =
      range($minimum, $maximum);
    $counter =
      count($array);

    for( $i = ( count($array) - 1 ); ( $i > 0 ); $i-- )
    { $alea =
        random_int(0, $i);
      list($array[$i], $array[$alea]) =
        array($array[$alea], $array[$i]);
    }

    return
      $this->ternary
        ( ( $number_number > $counter )
        , array_slice($array, 0, $counter)
        , array_slice($array, 0, $number_number)
        );
  }
  function captcha ( $title = null )
  { $headers =
      range('A', 'Z');
    $column_number =
      random_int(3, 4);
    $row_number =
      random_int(3, 4);
    $column_title =
      range('A', $headers[( $column_number - 1 )]);
    $row_title = 
      range('Z', $headers[( count($headers) - $row_number )]);
    $minimum =
      0;
    $maximum =
      99;
    $cell_number =
      ( $column_number * $row_number );
    $column_paragraph_prefix =
      ( $this->ternary
          ( $this->exists($title)
          , ( "<table>\n  <caption>"
            . $this->webify($title)
            . '</caption>'
            )
          , '<table>'
          )
        . "\n  <tr>\n    <td>&nbsp;</td>\n    <th scope=\"col\">"
      );
    $column =
      $this->paragraph
        ( $column_title
        , "</th>\n    <th scope=\"col\">"
        , $column_paragraph_prefix
        , "</th>\n  </tr>\n"
        );
    $lottery =
      $this->lotto($cell_number, $minimum, $maximum);
    $digit_number =
      mb_strlen('' . $maximum);
    
    foreach ( $lottery as $cell )
    { $lottery_textified[] =
        sprintf("%0{$digit_number}d", $cell);
    }

    $cursor_chosen =
      random_int(0, ( $cell_number - 1 ));
    $chosen =
      $lottery_textified[$cursor_chosen];
    $column_chosen =
      $column_title[( $cursor_chosen % $column_number )];
    $row_chosen =
      $row_title[intdiv($cursor_chosen, $column_number)];
    $lottery_chunked =
      array_chunk($lottery_textified, $column_number);
    $row_lottery =
      array_combine($row_title, $lottery_chunked);
    $row = '';

    foreach ( $row_lottery as $title => $chunk )
    { $row .=
        $this->paragraph
          ( $chunk
          , "</td>\n    <td>"
          , "  <tr>\n    <th scope=\"row\">$title</th>\n    <td>"
          , "</td>\n  </tr>\n"
          );
    }

    return
      array
        ( 'chosen' =>
            $chosen
        , 'column' =>
            $column_chosen
        , 'row' =>
            $row_chosen
        , 'matrix' =>
            ( $column . $row . "</table>\n" )
        );
  }
}
?>
